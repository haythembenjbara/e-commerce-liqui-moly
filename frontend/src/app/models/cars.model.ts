export interface cars {
  _id?: string;
  Make: string;
  Model: string;
  Category: string;
  Year: number;
}
