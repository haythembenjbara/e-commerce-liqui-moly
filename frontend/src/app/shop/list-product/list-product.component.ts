import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import {
  MatBottomSheet,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { MatSelectionList } from '@angular/material/list';
import { Store } from '@ngrx/store';
import {
  first,
  map,
  Observable,
  of,
  Subject,
  switchMap,
  takeUntil,
} from 'rxjs';
import { querySelector } from 'src/app/client/services/client.selectors';
import { ProductService } from 'src/app/core/services/product.service';
import { cars_category } from 'src/app/models/cars-category.model';
import { car_parts } from 'src/app/models/cars-parts.model';
import { cars } from 'src/app/models/cars.model';
import { AddCarDialogComponent } from '../add-car-dialog/add-car-dialog.component';
import { ShopService } from '../services/shop.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { removeQuery } from 'src/app/client/services/client.actions';
@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.sass'],
})
export class ListProductComponent implements OnInit, OnDestroy {
  public mobileQuery!: MediaQueryList;
  public mobileFilterOpen: boolean = false;
  products!: Observable<car_parts[]>;
  allCategorys!: cars_category[];
  allCars: cars[] = [];
  allBrands!: Observable<string[]>;
  filterByProduct!: car_parts | string | null;
  allFilters: {
    category: cars_category[];
    subCategory: string[];
    car: cars[];
    price: { max: number; min: number };
    brand: string[];
  } = {
    category: [],
    subCategory: [],
    car: [],
    price: { max: 0, min: 0 },
    brand: [],
  };
  private shop_filter_ref!: MatBottomSheetRef<any>;
  private _destroyed: Subject<boolean> = new Subject();
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private _bottomSheet: MatBottomSheet,
    private store: Store,
    private productService: ProductService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  private _mobileQueryListener: () => void;

  ngOnInit(): void {
    this.getCategorys();
  }
  ngOnDestroy(): void {
    this.store.dispatch(removeQuery());
    this._destroyed.next(true);
    this._destroyed.complete();
  }

  private getProducts(): void {
    this.getQuery()
      .pipe(takeUntil(this._destroyed))
      .subscribe({
        next: ({ query_type, value, more_info }) =>
          (this.products = this.querySwitch(query_type, value, more_info)),
      });
  }

  private getCategorys(): void {
    this.productService
      .getProducts_Categorys()
      .pipe(
        map((allcat) => allcat.map(({ cars_part, ...rest }) => rest)),
        first()
      )
      .subscribe({
        next: (res) => {
          this.allCategorys = res;
          this.getProducts();
          this.allBrands = this.productService.getCarMake();
        },
      });
  }
  private getQuery(): Observable<{
    query_type: 'CARS' | 'CATEGORY' | 'CAR_PARTS' | null;
    value: cars | car_parts | cars_category | string | null;
    more_info?: string | null;
  }> {
    return this.store.select(querySelector);
  }
  private querySwitch(
    query_type: 'CARS' | 'CATEGORY' | 'CAR_PARTS' | null,
    value: cars | car_parts | cars_category | string | null,
    more_info?: string | null
  ): Observable<car_parts[]> {
    if (!query_type || !value) {
      return this.productService.getAllProducts();
    }
    if (query_type === 'CARS') {
      const checkCar = this.allCars.some(
        (car) => car._id === (value as cars)._id
      );
      if (!checkCar) {
        this.allCars.push(value as cars);
        this.allFilters.car.push(value as cars);
      }
      return this.productService.getProducts_ByCars(value as cars);
    }
    if (query_type === 'CAR_PARTS') {
      this.filterByProduct = value as car_parts | string;
      if (
        Object.keys(value).some((key) => key === 'name' || key === 'quantity')
      ) {
        return of([value as car_parts]);
      }
      return this.productService.getProductByName(value as string);
    }
    if (query_type === 'CATEGORY') {
      let categoryIndex = this.allCategorys.findIndex(
        (ca) => ca._id === (value as cars_category)._id
      );
      this.allCategorys[categoryIndex] = value as cars_category;
      this.allFilters.category.push(value as cars_category);
      if (more_info) {
        this.allFilters.subCategory.push(more_info);
        return this.productService.getProductsBySubCategory(more_info);
      }
      return this.productService.getProductsByCategory(
        (value as cars_category)._id as string
      );
    }
    return this.productService.getAllProducts();
  }

  openMobileFilter(): void {
    this.mobileFilterOpen = true;
    this.shop_filter_ref = this._bottomSheet.open(ShopFilterComponent, {
      hasBackdrop: true,
      autoFocus: true,
      disableClose: true,
      closeOnNavigation: true,
    });
    this.shop_filter_ref.instance.products = this.products;
    this.shop_filter_ref.instance.allFilters = this.allFilters;
    this.shop_filter_ref.instance.allCategorys = this.allCategorys;
    this.shop_filter_ref.instance.allBrands = this.allBrands;
    this.shop_filter_ref.instance.allCars = this.allCars;
    this.shop_filter_ref.instance.isMobile = true;
    this.shop_filter_ref.instance.filterByProduct = this.filterByProduct;
    this.shop_filter_ref
      .afterDismissed()
      .pipe(first())
      .subscribe({
        next: (products) =>
          products
            ? ((this.products = products), (this.mobileFilterOpen = false))
            : null,
      });
  }
  closeMobileFilter(data: any): void {
    this.products = data.products;
    this.allFilters = data.allFilters;
    this.filterByProduct = data.filterByProduct;
  }
}

@Component({
  selector: 'shop-filter-component',
  templateUrl: 'shop-filter-component.html',
})
export class ShopFilterComponent implements OnInit {
  @Input('product') public products!: Observable<car_parts[]>;
  @Input('allCategorys') public allCategorys!: cars_category[];
  @Input('allCars') public allCars: cars[] = [];
  @Input('allBrands') public allBrands!: Observable<string[]>;
  @Input('isMobile') public isMobile: boolean = false;
  @Input('filterByProduct') public filterByProduct!: car_parts | string | null;
  @Input('allFilters') public allFilters: {
    category: cars_category[];
    subCategory: string[];
    car: cars[];
    price: { max: number; min: number };
    brand: string[];
  } = {
    category: [],
    subCategory: [],
    car: [],
    price: { max: 0, min: 0 },
    brand: [],
  };
  @Output('transfer_data') public transfer_data = new EventEmitter<{
    products: Observable<car_parts[]>;
    allFilters: any;
    filterByProduct: car_parts | string | null;
  }>();
  @ViewChild('carsSelect')
  carsSelect!: MatSelectionList;
  @ViewChild('brandsSelect') brandsSelect!: MatSelectionList;
  @ViewChild('categorySelect') categorySelect!: MatSelectionList;
  @ViewChild('subCategorySelect') subCategorySelect!: MatSelectionList;
  constructor(
    private shopService: ShopService,
    private _childBottomSheet: MatBottomSheet,
    @Optional()
    private ShopFilterComponent: MatBottomSheetRef<ShopFilterComponent>,
    private store: Store<any>
  ) {}

  ngOnInit(): void {}

  closeFilterModal(): void {
    this.ShopFilterComponent.dismiss(this.products);
  }

  applyFilter(): void {
    if (this.allFilters.car.length > 0) {
      this.brandsSelect.deselectAll();
      this.brandsSelect.disabled = true;
    }
    this.products = this.shopService.fullFilter(this.allFilters);
    if (!this.isMobile) {
      this.transfer_data.emit({
        products: this.products,
        allFilters: this.allFilters,
        filterByProduct: this.filterByProduct,
      });
    }
  }
  resetFilter(): void {
    this.allFilters = {
      category: [],
      subCategory: [],
      car: [],
      price: { max: 0, min: 0 },
      brand: [],
    };
  }
  removeSubCategory() {
    if (this.allFilters.subCategory.length > 0) {
      this.allFilters.subCategory = [];
      this.allFilters.category = [this.allFilters.category[0]];
    }
  }
  removeFilter(
    more:
      | 'CAR'
      | 'CATEGORY'
      | 'SUBCATEGORY'
      | 'BRAND'
      | 'MAX'
      | 'MIN'
      | 'PRODUCT',
    index?: number
  ): void {
    if (more === 'PRODUCT') {
      this.filterByProduct = null;
      this.store.dispatch(removeQuery());
      this.applyFilter();
      return;
    }
    if (more === 'CAR') {
      this.carsSelect.selectedOptions.deselect(
        this.carsSelect.selectedOptions.selected[index as number]
      );
      return;
    }
    if (more === 'CATEGORY') {
      this.categorySelect.selectedOptions.deselect(
        this.categorySelect.selectedOptions.selected[index as number]
      );
      this.allFilters.category = [];
      this.subCategorySelect.deselectAll();
      return;
    }
    if (more === 'SUBCATEGORY') {
      this.subCategorySelect.selectedOptions.deselect(
        this.subCategorySelect.selectedOptions.selected[index as number]
      );
    }
    if (more === 'BRAND') {
      this.brandsSelect.selectedOptions.deselect(
        this.brandsSelect.selectedOptions.selected[index as number]
      );
      return;
    }
    if (more === 'MAX') {
      this.allFilters.price.max = 0;
      return;
    }
    if (more === 'MIN') {
      this.allFilters.price.min = 0;
      return;
    }
  }
  addCar(): void {
    this._childBottomSheet
      .open(AddCarDialogComponent, {
        autoFocus: true,
      })
      .afterDismissed()
      .pipe(first())
      .subscribe({
        next: (result) => (result ? this.allCars.push(result) : null),
      });
  }
}
