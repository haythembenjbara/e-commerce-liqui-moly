import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'fileUrl',
})
export class FileUrlPipe implements PipeTransform {
  transform(url: string): string {
    return `${environment.server_url}/${url}`;
  }
}
