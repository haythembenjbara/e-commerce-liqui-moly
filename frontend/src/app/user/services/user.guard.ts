import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, switchMap } from 'rxjs';
import { AuthSelectors } from 'src/app/auth/services/auth.selectors';

@Injectable({
  providedIn: 'root',
})
export class UserGuard implements CanActivate {
  constructor(private store: Store<any>, private router: Router) {}
  canActivate(): Observable<boolean | UrlTree> {
    return this.store
      .select(AuthSelectors)
      .pipe(
        switchMap((user) =>
          user ? of(true) : of(this.router.parseUrl('/client'))
        )
      );
  }
}
