import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthModule } from '../auth/auth.module';
import { ProductComponent } from './product/product.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { AdminSideBarComponent } from './admin-side-bar/admin-side-bar.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './services/product.service';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CartModule } from '../cart/cart.module';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FileUrlPipe } from '../file-url.pipe';
@NgModule({
  declarations: [
    NavbarComponent,
    AdminSideBarComponent,
    ProductComponent,
    FooterComponent,
    FileUrlPipe,
  ],
  imports: [
    CommonModule,
    AuthModule,
    CartModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    RouterModule,
    HttpClientModule,
    CarouselModule,
    FormsModule,
    MatCheckboxModule,
    MatBottomSheetModule,
    MatAutocompleteModule,
  ],
  exports: [
    NavbarComponent,
    AdminSideBarComponent,
    FooterComponent,
    ProductComponent,
    FileUrlPipe,
  ],
  providers: [ProductService],
})
export class CoreModule {}
