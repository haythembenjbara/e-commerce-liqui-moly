import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-side-bar',
  templateUrl: './admin-side-bar.component.html',
  styleUrls: ['./admin-side-bar.component.sass'],
})
export class AdminSideBarComponent implements OnInit, OnDestroy {
  mobileQuery!: MediaQueryList;

  constructor() {}
  ngOnDestroy(): void {}
  ngOnInit(): void {}
}
