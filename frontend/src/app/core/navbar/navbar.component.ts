import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subject, first, takeUntil } from 'rxjs';
import { AuthSelectors } from 'src/app/auth/services/auth.selectors';
import { queryProductBy } from 'src/app/client/services/client.actions';
import { car_parts } from 'src/app/models/cars-parts.model';
import { cars } from 'src/app/models/cars.model';
import { user_shop } from 'src/app/models/user-shop.model';
import { AddCarDialogComponent } from 'src/app/shop/add-car-dialog/add-car-dialog.component';
import { ProductService } from '../services/product.service';
import { SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass', './navbar.component.css'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  mobileSearchBarState: boolean = false;
  searchBar: string = '';
  user!: user_shop | null;
  allCars!: cars[];
  checkCars!: Array<{ checked: boolean; index: number }>;
  sugestionProducts!: car_parts[];
  product_image!: Observable<SafeUrl> | Observable<SafeUrl>[];
  private readonly destroyed = new Subject<boolean>();
  constructor(
    private router: Router,
    private store: Store<any>,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.auth_state();
  }
  ngOnDestroy(): void {
    this.destroyed.next(true);
    this.destroyed.complete();
  }
  searchByString(): void {
    this.store.dispatch(
      queryProductBy({ query_type: 'CAR_PARTS', value: this.searchBar })
    );
    this.sugestionProducts = [];
    if (this.router.url.match(/list-product/)) {
      return;
    }
    this.router.navigate(['shop']);
  }
  productSelected(event: MatAutocompleteSelectedEvent): void {
    this.store.dispatch(
      queryProductBy({ query_type: 'CAR_PARTS', value: event.option.value })
    );
    this.sugestionProducts = [];
    if (this.router.url.match(/list-product/)) {
      return;
    }
    this.router.navigate(['shop']);
  }

  findProducts(): void {
    if (this.searchBar === '') {
      return;
    }
    this.productService
      .getProductByName(this.searchBar)
      .pipe(first())
      .subscribe({
        next: (result) => (
          (this.sugestionProducts = result), this._generateProductsImage()
        ),
      });
  }
  mobileSearchBar(): void {
    // OPEN
    if (!this.mobileSearchBarState) {
      this.mobileSearchBarState = true;
      return;
    }
    // CLOSE
    if (this.mobileSearchBarState) {
      this.mobileSearchBarState = false;
    }
  }
  auth_state(): void {
    this.store
      .select(AuthSelectors)
      .pipe(takeUntil(this.destroyed))
      .subscribe({
        next: (user) => (
          (this.user = user),
          user
            ? ((this.allCars = user.cars), this._generateCheckBox())
            : (this.allCars = [])
        ),
        error: (err) => console.error(err),
      });
  }
  async queryByCar(
    event: MatCheckboxChange,
    car: cars,
    i: number
  ): Promise<void> {
    if (!event.checked) {
      return;
    }
    this.checkCars = this.checkCars.map((elem, index) =>
      index === i ? elem : { checked: false, index: index }
    );
    this.store.dispatch(queryProductBy({ query_type: 'CARS', value: car }));
    if (this.router.url.match(/list-product/)) {
      return;
    }
    await this.router.navigate(['/shop']);
  }
  displayFn(product: car_parts): string {
    return product && product.name ? product.name.toUpperCase() : '';
  }
  private _generateCheckBox(): void {
    this.checkCars = this.allCars.map((_, index) => ({
      checked: false,
      index: index,
    }));
  }
  private _generateProductsImage() {
    this.product_image = this.sugestionProducts.map((product) =>
      this.productService.getStaticFile(product.image_urls[0] as string)
    );
  }
}
