import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { first } from 'rxjs';
import { AuthModule } from '../auth.module';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  standalone: true,
  imports: [
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,

    MatSnackBarModule,
    AuthModule,
  ],
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.sass'],
})
export class AdminLoginComponent implements OnInit {
  public username!: string;
  public password!: string;
  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login(): void {
    if (this.username === '' || this.password === '') {
      return;
    }
    this.authService
      .admin_login(this.username, this.password)
      .pipe(first())
      .subscribe({
        next: (res) => this.handle_token(res),
        error: () => this._print_err(),
      });
  }
  private _print_err(): void {
    this.username = '';
    this.password = '';
    this.snackBar.open('informations non valides'.toUpperCase(), 'Dismiss', {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
  private handle_token(token: any): void {
    localStorage.setItem('SCODEP_TOKEN', token.access_token);
    this.router.navigateByUrl('admin');
  }
}
