import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  AfterContentInit,
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, first } from 'rxjs';
import { cars_category } from 'src/app/models/cars-category.model';
import { AdminServiceService } from '../services/admin-service.service';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.sass'],
})
export class AddCategoryComponent implements OnInit {
  @Input('category') category!: cars_category;
  imagePreview!: any;
  categoryImage!: File | null;
  sub_category: string[] = [];
  category_name!: string;

  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  constructor(
    private adminService: AdminServiceService,
    private snackBar: MatSnackBar,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this._patch_category();
  }

  async addFile(event: any) {
    this.categoryImage = event.target.files[0];
    this.imagePreview = await this._make_image_preview(event.target.files[0]);
  }
  removeFile() {
    this.imagePreview = null;
    this.categoryImage = null;
  }
  removeCategory(subCategory: string) {
    const index = this.sub_category.indexOf(subCategory);
    if (index >= 0) {
      this.sub_category.splice(index, 1);
    }
  }

  addCategory(event: MatChipInputEvent) {
    const value = (event.value || '').trim();
    if (value) {
      this.sub_category.push(value);
    }
    event.chipInput!.clear();
  }
  submit() {
    if (this.categoryImage === null) {
      this.snackBar.open('VOUS DEVEZ CHOISIR UNE IMAGE', 'Dismiss', {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
      return;
    }
    let cars_category = new FormData();
    cars_category.append('name', this.category_name);
    cars_category.append('image_url', this.categoryImage);
    this.sub_category.map((sub) => cars_category.append('sub_category[]', sub));
    let api!: Observable<any>;
    if (this.category) {
      api = this.adminService
        .edit_cars_category(this.category._id as string, cars_category)
        .pipe(first());
    } else {
      api = this.adminService.addCarsCategory(cars_category).pipe(first());
    }
    api.subscribe({
      next: () =>
        this.snackBar.open('SUCCESS', 'Dismiss', {
          duration: 2000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
        }),
      error: () => {
        this.snackBar.open('NOM DEJA UTILISER', 'Dismiss', {
          duration: 2000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      },
    });
  }

  private _make_image_preview(image: File): Promise<any> {
    let file_reader = new FileReader();
    return new Promise((resolve, reject) => {
      file_reader.onload = (event) => resolve(event.target?.result);
      file_reader.onerror = (err) => reject(err);
      file_reader.readAsDataURL(image);
    });
  }
  private _patch_category(): void {
    if (!this.category) {
      return;
    }
    this.category_name = this.category.name;
    this.sub_category = this.category.sub_category
      ? this.category.sub_category
      : [];
    this.productService
      .getStaticFile(this.category.image_url as string)
      .subscribe({ next: (file) => (this.imagePreview = file) });
  }
}
