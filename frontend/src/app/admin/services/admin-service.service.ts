import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { user_shop } from 'src/app/models/user-shop.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdminServiceService {
  constructor(private http: HttpClient) {}

  public add_cars_parts(car_part: FormData): Observable<any> {
    return this.http.post(
      environment.server_url + '/admin/addCarParts',
      car_part,
      {
        responseType: 'text',
      }
    );
  }
  public addCarsCategory(cars_category: FormData): Observable<any> {
    return this.http.post(
      environment.server_url + '/admin/addCarCategory',
      cars_category,
      {
        responseType: 'text',
      }
    );
  }
  public get_all_orders(): Observable<user_shop[]> {
    return this.http.get<user_shop[]>(
      `${environment.server_url}/admin/get_orders`
    );
  }
  public edit_order_state(
    user_id: string,
    order_id: string,
    order_state: string
  ): Observable<any> {
    return this.http.put(
      `${environment.server_url}/admin/edit_order_state`,
      {
        user_id,
        order_id,
        order_state,
      },
      {
        responseType: 'text',
      }
    );
  }
  public edit_cars_part(id: string, car_part: FormData): Observable<any> {
    return this.http.post(
      `${environment.server_url}/admin/edit_car_parts/${id}`,
      car_part,
      { responseType: 'text' }
    );
  }
  public delete_cars_part(id: string): Observable<any> {
    return this.http.delete(
      `${environment.server_url}/admin/delete_car_parts/${id}`,
      {
        responseType: 'text',
      }
    );
  }
  edit_cars_category(
    category_id: string,
    cars_category: FormData
  ): Observable<any> {
    return this.http.post(
      `${environment.server_url}/admin/edit_cars_category/${category_id}`,
      cars_category,
      { responseType: 'text' }
    );
  }
  delete_cars_category(category_id: string): Observable<string> {
    return this.http.delete(
      `${environment.server_url}/admin/delete_car_category/${category_id}`,
      { responseType: 'text' }
    );
  }
}
