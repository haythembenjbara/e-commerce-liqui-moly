import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../services/admin-service.service';
import { Observable, first } from 'rxjs';
import { order, user_shop } from 'src/app/models/user-shop.model';
import { SafeUrl } from '@angular/platform-browser';
import { ProductService } from 'src/app/core/services/product.service';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { FileSaverService } from 'ngx-filesaver';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.sass'],
})
export class OrderListComponent implements OnInit {
  allUser!: user_shop[];
  cart_productsImages!: Observable<SafeUrl>[];
  orders_states!: number[];
  sortBy: 'USER' | 'ORDER' = 'USER';
  findByUserName!: string;
  filterdUsers!: user_shop[];
  orderSortBy: 'ALL' | 'ACTIVE' | 'FINISHED' = 'ALL';
  allOrders!: order[];
  active_orders!: order[];
  finished_orders!: order[];
  constructor(
    private adminService: AdminServiceService,
    private productService: ProductService,
    private fileSaverService: FileSaverService
  ) {}

  ngOnInit(): void {
    this._get_all_users();
  }
  public findByUser(): void {
    if (!this.findByUserName) {
      this.filterdUsers = this.allUser;
      return;
    }
    let regex = new RegExp(this.findByUserName);
    this.filterdUsers = this.allUser.filter(
      (user) => user.displayName.match(regex) || user.email.match(regex)
    );
  }
  public change_order_state(
    user_id: string,
    order_id: string,
    event: StepperSelectionEvent,
    order_index: number
  ): void {
    const orderState =
      event.selectedIndex === 0
        ? 'EN ATTENTE DE LIVRAISON'
        : event.selectedIndex === 1
        ? 'EN COURS DE LIVRAISON'
        : 'LIVRAISON TERMINER';
    this.orders_states[order_index] = event.selectedIndex;
    this.adminService
      .edit_order_state(user_id, order_id, orderState)
      .pipe(first())
      .subscribe({
        next: () => null,
      });
  }
  public saveInvoice(
    user_name: string,
    order_id: string,
    invoice_url: string
  ): void {
    this.productService
      .getInvoicePdf(invoice_url)
      .pipe(first())
      .subscribe({
        next: (res) =>
          this.fileSaverService.save(
            res.file,
            `facture-${user_name}-commande-${order_id}.pdf`
          ),
      });
  }
  private _get_all_users(): void {
    this.adminService
      .get_all_orders()
      .pipe(first())
      .subscribe({
        next: (users) => (
          (this.allUser = users),
          (this.filterdUsers = users),
          this.generateProductsImage(),
          this.arrangeOrdersState(),
          this.arrange_orders()
        ),
      });
  }
  private generateProductsImage(): void {
    this.cart_productsImages = this.allUser.flatMap((user) =>
      user.orders.flatMap((order) =>
        order.order.map((product) =>
          this.productService.getStaticFile(
            product.car_part.image_urls[0] as string
          )
        )
      )
    );
  }
  private arrangeOrdersState(): void {
    this.orders_states = this.allUser.flatMap((user) => {
      return user.orders.map((order) => {
        if (order.orderStatus === 'EN ATTENTE DE LIVRAISON') {
          return 0;
        }
        if (order.orderStatus === 'EN COURS DE LIVRAISON') {
          return 1;
        }
        return 2;
      });
    });
  }
  private arrange_orders(): void {
    this.allOrders = this.allUser.flatMap((user) =>
      user.orders.map((order) => ({
        ...order,
        user_custom_information: {
          email: user.email,
          phoneNumber: user.phoneNumber,
        },
      }))
    );
    this.active_orders = this.allOrders.filter(
      (order) =>
        order.orderStatus === 'EN ATTENTE DE LIVRAISON' ||
        order.orderStatus === 'EN COURS DE LIVRAISON'
    );
    this.finished_orders = this.allOrders.filter(
      (order) => order.orderStatus === 'LIVRAISON TERMINER'
    );
  }
}
