import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.sass'],
})
export class AdminDashboardComponent implements OnInit {
  active_menu!: 'ORDER' | 'PRODUCT' | 'CATEGORY' | 'CARS' | '';
  constructor() {}

  ngOnInit(): void {}

  generateMenu(
    menu: 'ORDER' | 'PRODUCT' | 'CATEGORY' | 'CARS',
    type: 'ADD' | 'REMOVE'
  ): void {
    if (type === 'REMOVE') {
      const elem = document.querySelector('.menu-active');
      elem?.classList.remove('menu-active');
      elem?.classList.add('menu-not-active');
      this.active_menu = '';
      return;
    }
    const oldMenu = document.querySelector('.menu-active');
    oldMenu?.classList.remove('menu-active');
    oldMenu?.classList.add('menu-not-active');
    const elem = document.querySelector(`#${menu}`);
    elem?.classList.add('menu-active');
    elem?.classList.remove('menu-not-active');
    this.active_menu = menu;
  }
}
