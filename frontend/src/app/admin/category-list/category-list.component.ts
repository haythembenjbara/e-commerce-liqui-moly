import { Component, OnInit } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { Observable, first } from 'rxjs';
import { cars_category } from 'src/app/models/cars-category.model';
import { AdminServiceService } from '../services/admin-service.service';
import { ProductService } from 'src/app/core/services/product.service';
import { MatDialog } from '@angular/material/dialog';
import { AddCategoryComponent } from '../add-category/add-category.component';
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.sass'],
})
export class CategoryListComponent implements OnInit {
  public allCategory!: cars_category[];
  public allImages!: Observable<SafeUrl>[];
  constructor(
    private adminService: AdminServiceService,
    private productService: ProductService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAllCategory();
  }
  openModal(category?: cars_category): void {
    const modalInstance = this.dialog.open(AddCategoryComponent, {
      width: '100%',
    });
    if (category) {
      modalInstance.componentInstance.category = category;
    }
    modalInstance
      .afterClosed()
      .pipe(first())
      .subscribe(() => this.getAllCategory());
  }
  deleteCategory(category: cars_category): void {
    const result = confirm('Êtes-vous sur de vouloir continuer?');
    if (!result) {
      return;
    }
    this.adminService
      .delete_cars_category(category._id as string)
      .pipe(first())
      .subscribe(() => this.getAllCategory());
  }
  private getAllCategory(): void {
    this.productService
      .getProducts_Categorys()
      .pipe(first())
      .subscribe({
        next: (category) => (
          (this.allCategory = category), this.getCategoryImage()
        ),
      });
  }
  private getCategoryImage(): void {
    this.allImages = this.allCategory.map((category) =>
      this.productService.getStaticFile(category.image_url as string)
    );
  }
}
