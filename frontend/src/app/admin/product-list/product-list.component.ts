import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SafeUrl } from '@angular/platform-browser';
import { Observable, first } from 'rxjs';
import { ProductService } from 'src/app/core/services/product.service';
import { car_parts } from 'src/app/models/cars-parts.model';
import { ShopService } from 'src/app/shop/services/shop.service';
import { AddCarsPartsComponent } from '../add-cars-parts/add-cars-parts.component';
import { AdminServiceService } from '../services/admin-service.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.sass'],
})
export class ProductListComponent implements OnInit {
  public allProducts!: car_parts[];
  public filterProducts!: car_parts[];
  public searchBar!: string;
  public allImages!: Observable<SafeUrl>[];
  public scrollContent!: car_parts[];

  constructor(
    private productService: ProductService,
    private adminService: AdminServiceService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.gelAllProducts();
  }
  openModal(product?: car_parts): void {
    const modalInstance = this.dialog.open(AddCarsPartsComponent, {
      width: '100%',
      height: '80%',
    });
    if (product) {
      modalInstance.componentInstance.product = product;
    }
    modalInstance
      .afterClosed()
      .pipe(first())
      .subscribe((res) => (res ? this.patchProduct(res) : null));
  }
  filter(): void {
    if (this.searchBar && this.searchBar.length > 0) {
      this.filterProducts = this.allProducts.filter((product) =>
        product.name.match(this.searchBar.toLowerCase())
      );
    } else {
      this.filterProducts = this.allProducts;
    }
  }
  deleteProduct(product: car_parts): void {
    const result = confirm('Êtes-vous sur de vouloir continuer?');
    if (!result) {
      return;
    }
    this.adminService
      .delete_cars_part(product._id as string)
      .pipe(first())
      .subscribe({ next: () => this.patch_remove_product(product) });
  }
  track_products(index: number, product: car_parts): string {
    return product._id as string;
  }
  private getProductImage(): void {
    this.allImages = this.allProducts.map((product) =>
      this.productService.getStaticFile(product.image_urls[0] as string)
    );
  }
  private gelAllProducts(): void {
    this.productService
      .getAllProducts()
      .pipe(first())
      .subscribe({
        next: (products) => (
          ((this.allProducts = products),
          ((this.filterProducts = products),
          this.scrollAdvance({ initState: true }))),
          this.getProductImage()
        ),
      });
  }
  private patchProduct({
    product_name,
    product_id,
  }: {
    product_name?: string;
    product_id?: string;
  }): void {
    if (product_id) {
      this.productService
        .getProductById(product_id)
        .pipe(first())
        .subscribe({
          next: (product) => this.patch_edit_product(product),
          error: (err) => console.error(err),
        });
      return;
    }

    this.productService
      .getProductByName(product_name as string)
      .pipe(first())
      .subscribe({
        next: (product) => this.patch_add_product(product[0]),
        error: (err) => console.error(err),
      });
  }
  private patch_edit_product(product: car_parts): void {
    this.searchBar = '';
    const findProduct = this.filterProducts.findIndex(
      (prod) => prod._id === product._id
    );
    if (findProduct !== 1) {
      this.filterProducts[findProduct] = product;
      this.allImages[findProduct] = this.productService.getStaticFile(
        product.image_urls[0] as string
      );
    }
  }
  private patch_add_product(product: car_parts): void {
    this.searchBar = '';
    this.filterProducts.push(product);
    this.allImages.push(
      this.productService.getStaticFile(product.image_urls[0] as string)
    );
  }
  private patch_remove_product(product: car_parts): void {
    this.searchBar = '';
    const findProduct = this.filterProducts.findIndex(
      (prod) => prod._id === product._id
    );
    if (findProduct === -1) {
      return;
    }
    this.filterProducts.splice(findProduct, 1);
    this.allImages.splice(findProduct, 1);
  }
  @HostListener('window:scroll', [])
  private checkScrollState(): void {
    let pos =
      (document.documentElement.scrollTop || document.body.scrollTop) +
      document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    if (pos == max) {
      this.scrollAdvance({ initState: false });
    }
  }
  private scrollAdvance({ initState }: { initState: boolean }): void {
    if (initState) {
      return;
    }
  }
}
