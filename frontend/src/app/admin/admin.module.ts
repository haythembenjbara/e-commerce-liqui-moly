import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AddCarsPartsComponent } from './add-cars-parts/add-cars-parts.component';
import { CoreModule } from '../core/core.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ShopModule } from '../shop/shop.module';
import { MatButtonModule } from '@angular/material/button';
import { AdminServiceService } from './services/admin-service.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AddCategoryComponent } from './add-category/add-category.component';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatListModule } from '@angular/material/list';
import { OrderListComponent } from './order-list/order-list.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { ProductListComponent } from './product-list/product-list.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { CategoryListComponent } from './category-list/category-list.component';
import { CarListComponent } from './car-list/car-list.component';
import { AuthModule } from '../auth/auth.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './services/token.interceptor';
@NgModule({
  declarations: [
    AddCarsPartsComponent,
    AddCategoryComponent,
    AdminDashboardComponent,
    OrderListComponent,
    ProductListComponent,
    CategoryListComponent,
    CarListComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CoreModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    ShopModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDividerModule,
    MatCardModule,
    MatProgressBarModule,
    MatListModule,
    MatExpansionModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatTabsModule,
    AuthModule,
  ],
  providers: [
    AdminServiceService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
})
export class AdminModule {}
