import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  OnDestroy,
  Input,
} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { first, forkJoin, Observable, Subject, takeUntil } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { cars } from 'src/app/models/cars.model';
import { ShopService } from 'src/app/shop/services/shop.service';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AdminServiceService } from '../services/admin-service.service';
import { cars_category } from 'src/app/models/cars-category.model';
import { ProductService } from 'src/app/core/services/product.service';
import { car_parts } from 'src/app/models/cars-parts.model';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-cars-parts',
  templateUrl: './add-cars-parts.component.html',
  styleUrls: ['./add-cars-parts.component.sass'],
})
export class AddCarsPartsComponent implements OnInit, OnDestroy {
  @Input('product') product?: car_parts;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  selectedCars: cars[] = [];
  destroy: Subject<boolean> = new Subject<boolean>();
  suggestionCars!: cars[];
  all_images_upload: File[] = [];
  all_preview_images: any[] = [];
  allCategory!: cars_category[];
  sub_category!: string[];
  product_form!: FormGroup;
  errorMessage!: string | null;
  loader_spinner!: boolean;
  @ViewChild('carInput') carInput!: ElementRef<HTMLInputElement>;
  @ViewChild('fileInput') fileInput!: ElementRef<HTMLInputElement>;

  constructor(
    private shopService: ShopService,
    private adminService: AdminServiceService,
    private productService: ProductService,
    private _formBuilder: FormBuilder,
    public matDialogRef: MatDialogRef<AddCarsPartsComponent>,
    private snackBar: MatSnackBar
  ) {
    this.product_form = this._formBuilder.group({
      first_step: this._formBuilder.group({
        name: ['', Validators.required],
        category: new FormControl<cars_category | null>(
          null,
          Validators.required
        ),
        sub_category: new FormControl<string[] | null>([]),
      }),
      second_step: this._formBuilder.group({
        price: new FormControl<number>(0, Validators.required),
        quantity: new FormControl<number>(1, Validators.required),
        description: ['', Validators.required],
      }),
      third_step: this._formBuilder.array([
        this._formBuilder.group({
          feature_name: ['', Validators.required],
          feature_value: ['', Validators.required],
        }),
      ]),
    });
  }
  get first_step_control() {
    return this.product_form.get('first_step') as FormGroup;
  }
  get second_step_control() {
    return this.product_form.get('second_step') as FormGroup;
  }
  get third_step_control() {
    return this.product_form.get('third_step') as FormArray;
  }
  get four_step_control() {
    return this.product_form.get('four_step') as FormArray;
  }
  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }
  ngOnInit(): void {
    this.loader_spinner = true;
    this.patch_product();
    this.productService
      .getProducts_Categorys()
      .pipe(first())
      .subscribe(
        (res) => (
          (this.allCategory = res),
          this.show_sub_categorie(),
          (this.loader_spinner = false)
        )
      );
  }

  remove(index: number): void {
    this.selectedCars.splice(index, 1);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.selectedCars.push(event.option.value);
    this.carInput.nativeElement.value = '';
    this.suggestionCars = [];
  }

  findCars(filter: string): void {
    this.shopService
      .filterCars(filter)
      .pipe(takeUntil(this.destroy))
      .subscribe({ next: (cars) => (this.suggestionCars = cars) });
  }
  async addFile(event: any): Promise<void> {
    this.all_images_upload = this.all_images_upload.concat(
      Array.from(event.target.files)
    );
    this.all_preview_images = await Promise.all(
      this.all_images_upload.map((image) => this._make_image_preview(image))
    );
  }

  removeFile(event: Event, index: number): void {
    event.stopPropagation();
    this.all_preview_images.splice(index, 1);
    this.all_images_upload.splice(index, 1);
    if (index === 0) {
      this.fileInput.nativeElement.value = '';
    }
  }

  addFeature(): void {
    if (this.third_step_control.valid) {
      const features = this._formBuilder.group({
        feature_name: ['', Validators.required],
        feature_value: ['', Validators.required],
      });
      this.third_step_control.push(features);
    }
  }
  submit(): void {
    let car_part = new FormData();
    // INSERT FIRST STEPS
    car_part.append(
      'name',
      this.product_form.get('first_step')?.get('name')?.value
    );
    car_part.append(
      'category',
      JSON.stringify(
        this.product_form.get('first_step')?.get('category')?.value
      )
    );
    this.product_form
      .get('first_step')
      ?.get('sub_category')
      ?.value.map((c: string) => car_part.append('sub_category[]', c));
    // INSERT SECOND STEP
    car_part.append(
      'price',
      this.product_form.get('second_step')?.get('price')?.value
    );
    car_part.append(
      'quantity',
      this.product_form.get('second_step')?.get('quantity')?.value
    );
    car_part.append(
      'description',
      this.product_form.get('second_step')?.get('description')?.value
    );
    // INSERT THIRD STEP
    car_part.append('insertion_date', Date.now().toString());
    this.product_form
      .get('third_step')
      ?.value.filter(
        (feature: { feature_name: string; feature_value: string }) =>
          feature.feature_name.length > 0 && feature.feature_value.length > 0
      )
      .map((feature: { feature_name: string; feature_value: string }) =>
        car_part.append('features[]', JSON.stringify(feature))
      );
    // INSERT FOURTH STEP
    this.selectedCars.map((car) =>
      car_part.append('cars[]', JSON.stringify(car))
    );
    // INSERT FIFH STEP
    this.all_images_upload.map((file) => car_part.append('image_urls', file));
    // INSERT PRODUCT
    this.adminService
      .add_cars_parts(car_part)
      .pipe(first())
      .subscribe({
        next: () => (this.errorMessage = null),
        error: (err) => {
          if (err.status === 409) {
            this.errorMessage = `LE PRODUIT AVEC LE NOM "${this.product_form
              .get('first_step')
              ?.get('name')
              ?.value.toUpperCase()}" Existe deja`;
            return;
          }
          this.errorMessage = 'ERREUR SERVEUR';
        },
      });
  }
  edit(): void {
    let car_part = new FormData();
    // INSERT FIRST STEPS
    car_part.append('_id', this.product?._id as string);
    car_part.append(
      'name',
      this.product_form.get('first_step')?.get('name')?.value
    );
    car_part.append(
      'category',
      JSON.stringify(
        this.product_form.get('first_step')?.get('category')?.value
      )
    );
    this.product_form
      .get('first_step')
      ?.get('sub_category')
      ?.value.map((c: string) => car_part.append('sub_category[]', c));
    // INSERT SECOND STEP
    car_part.append(
      'price',
      this.product_form.get('second_step')?.get('price')?.value
    );
    car_part.append(
      'quantity',
      this.product_form.get('second_step')?.get('quantity')?.value
    );
    car_part.append(
      'description',
      this.product_form.get('second_step')?.get('description')?.value
    );
    // INSERT THIRD STEP
    car_part.append('insertion_date', Date.now().toString());
    this.product_form
      .get('third_step')
      ?.value.filter(
        (feature: { feature_name: string; feature_value: string }) =>
          feature.feature_name.length > 0 && feature.feature_value.length > 0
      )
      .map((feature: { feature_name: string; feature_value: string }) =>
        car_part.append('features[]', JSON.stringify(feature))
      );
    // INSERT FOURTH STEP
    this.selectedCars.map((car) =>
      car_part.append('cars[]', JSON.stringify(car))
    );
    // INSERT FIFH STEP

    this.all_images_upload.map((file) => car_part.append('image_urls', file));
    this.adminService
      .edit_cars_part(this.product?._id as string, car_part)
      .pipe(first())
      .subscribe({
        next: () => this.closeModal(),
        error: () =>
          this.snackBar.open('NOM DEJA UTILISER', 'Dismiss', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
          }),
      });
  }
  compareCategory(o1: cars_category, o2: cars_category): boolean {
    return o1 && o2 ? o1.name === o2.name : o1 === o2;
  }
  closeModal(): void {
    this.matDialogRef.close({
      product_id: this.product ? (this.product._id as string) : null,
      product_name: this.first_step_control.get('name')?.value,
    });
  }
  private _make_image_preview(image: File): Promise<any> {
    let file_reader = new FileReader();
    return new Promise((resolve, reject) => {
      file_reader.onload = (event) => resolve(event.target?.result);
      file_reader.onerror = (err) => reject(err);
      file_reader.readAsDataURL(image);
    });
  }
  private show_sub_categorie(): void {
    this.product_form
      .get('first_step')
      ?.get('category')
      ?.valueChanges.pipe(takeUntil(this.destroy))
      .subscribe((val) => {
        if (val.sub_category && val.sub_category.length > 0) {
          this.sub_category = val.sub_category;
        } else {
          this.sub_category = [];
          this.product_form.get('first_step')?.get('sub_category')?.reset([]);
        }
      });
  }
  private async patch_product(): Promise<void> {
    if (!this.product) {
      return;
    }
    // FIRST ROW
    this.first_step_control.patchValue({
      name: this.product.name,
      category: this.product.category,
      sub_category: this.product.sub_category,
    });
    if (
      this.product.category.sub_category &&
      this.product.category.sub_category.length > 0
    ) {
      this.sub_category = this.product.category.sub_category;
    }
    // SECOND ROW
    this.second_step_control.patchValue({
      price: this.product.price,
      quantity: this.product.quantity,
      description: this.product.description,
    });
    // THIRD ROW
    this.third_step_control.clear();
    this.product.features.map((feature) => {
      const feature_form = this._formBuilder.group({
        feature_name: [feature.feature_name, Validators.required],
        feature_value: [feature.feature_value, Validators.required],
      });
      this.third_step_control.push(feature_form);
    });
    // FOUR ROW
    this.selectedCars = this.product.cars ? this.product.cars : [];
    // FIFTH ROW
    forkJoin(
      this.product.image_urls.map((url) =>
        this.productService.getBlobFile(url as string)
      )
    )
      .pipe(first())
      .subscribe(
        async (files) => (
          (this.all_images_upload = files.map(
            (file) => new File([file.blob], file.name)
          )),
          (this.all_preview_images = await Promise.all(
            files.map((file) =>
              this._make_image_preview(new File([file.blob], file.name))
            )
          ))
        )
      );
  }
}
