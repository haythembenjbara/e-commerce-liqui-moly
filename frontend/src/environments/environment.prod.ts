export const environment = {
  production: true,
  server_url: 'https://scodep-server.online:3000',
  firebase_config: {
    apiKey: 'AIzaSyCZXoEWYAqbrBGSSJeeO3itP2hWIZWeD20',
    authDomain: 'liquimoly-871fd.firebaseapp.com',
    projectId: 'liquimoly-871fd',
    storageBucket: 'liquimoly-871fd.appspot.com',
    messagingSenderId: '978005805119',
    appId: '1:978005805119:web:881300173640118e80108a',
    measurementId: 'G-V9NZM62J2T',
  },
  paymeeConfig: {
    api_url: 'https://sandbox.paymee.tn',
    token: 'c0d2bbb79dabe9eac33f9df4cb9fbca75433e291',
    accountNum: 2529,
  },
};
