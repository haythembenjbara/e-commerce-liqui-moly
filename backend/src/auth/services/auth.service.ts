import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CarParts } from '../../models/car-parts.model';
import { car } from '../../models/car.model';
import { user, UserDocument } from '../../models/user.model';

@Injectable()
export class AuthService {
  constructor(@InjectModel(user.name) private userModel: Model<UserDocument>) {}

  async login(user: user): Promise<{ message: string; user: user }> {
    if (user.provider === 'INTERNAL') {
      return this._internal_login(user);
    }
    return this._external_login(user);
  }

  async register(user: user): Promise<{ message: string; user: user }> {
    if (Object.keys(user).length === 0) {
      throw new HttpException('MISSING INFORMATION', HttpStatus.NOT_ACCEPTABLE);
    }
    const findUser = await this.userModel.findOne({ email: user.email });
    if (findUser) {
      throw new HttpException('USER ALEARDY EXIST', HttpStatus.FOUND);
    }
    await this.userModel.create(user);
    return { message: 'USER ADDED SUCCESSFULY', user: user };
  }

  async get_user(email: string): Promise<{ message: string; user: user }> {
    const findUser = await this.userModel
      .findOne({ email })
      .populate({ path: 'cart.car_part', model: CarParts.name })
      .populate({
        path: 'cars',
        model: car.name,
      })
      .select('-orders');
    if (!findUser) {
      throw new HttpException('NOT FOUND', HttpStatus.NOT_FOUND);
    }
    return { message: 'SUCCESS', user: findUser };
  }
  private async _internal_login(
    user: user,
  ): Promise<{ message: string; user: user }> {
    const findUser = await this.userModel.findOne({ email: user.email });
    if (!findUser) {
      throw new HttpException('NO USER FOUND', HttpStatus.NOT_FOUND);
    }
    if (findUser && Object.keys(findUser).length === 0) {
      throw new HttpException('MISSING INFORMATION', HttpStatus.NOT_ACCEPTABLE);
    }
    return { message: 'LOGIN SUCCESS', user: findUser };
  }
  private async _external_login(
    user: user,
  ): Promise<{ message: string; user: user }> {
    const findUser = await this.userModel.findOne({ email: user.email });
    if (!findUser && Object.values(user.adresse).every((v) => v === null)) {
      throw new HttpException('MISSING INFORMATION', HttpStatus.NOT_ACCEPTABLE);
    }
    if (findUser && Object.values(findUser.adresse).every((v) => v === null)) {
      throw new HttpException('MISSING INFORMATION', HttpStatus.NOT_ACCEPTABLE);
    }
    if (!findUser && Object.values(user.adresse).every((v) => v !== null)) {
      const newUser = await this.userModel.create(user);
      return { message: 'LOGIN SUCCESS', user: newUser };
    }
    return { message: 'LOGIN SUCCESS', user: findUser };
  }
}
