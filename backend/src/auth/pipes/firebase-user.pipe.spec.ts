import { FirebaseUserPipe } from './firebase-user.pipe';

describe('FirebaseUserPipe', () => {
  it('should be defined', () => {
    expect(new FirebaseUserPipe()).toBeDefined();
  });
});
