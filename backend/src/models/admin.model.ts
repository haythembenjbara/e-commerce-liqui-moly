import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type AdminDocument = admin & Document;

@Schema({ capped: { max: 1 } })
export class admin {
  @Prop({ required: true })
  username: string;
  @Prop({ required: true })
  password: string;
}
export const AdminSchema = SchemaFactory.createForClass(admin);
