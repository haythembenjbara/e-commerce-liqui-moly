import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { readFileSync } from 'fs';

const httpsOptions = {
  cert: readFileSync(
    '/etc/letsencrypt/live/scodep-server.online/fullchain.pem',
    'utf8',
  ),
  key: readFileSync(
    '/etc/letsencrypt/live/scodep-server.online/privkey.pem',
    'utf8',
  ),
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    httpsOptions: httpsOptions,
  });
  await app.listen(process.env.APP_PORT);
}
bootstrap();
