import { Module } from '@nestjs/common';
import { AdminServiceService } from './services/admin-service.service';
import { AdminController } from './admin.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { car, CarSchema } from '../models/car.model';
import { CarParts, CarPartsSchema } from '../models/car-parts.model';
import { CarCategory, CarCategorySchema } from '../models/car-category.model';
import { user, UserSchema } from 'src/models/user.model';
import { admin, AdminSchema } from 'src/models/admin.model';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './services/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './services/jwt.strategy';

@Module({
  imports: [
    ConfigModule,
    PassportModule,
    MongooseModule.forFeature([
      { name: car.name, schema: CarSchema },
      { name: CarParts.name, schema: CarPartsSchema },
      { name: CarCategory.name, schema: CarCategorySchema },
      { name: user.name, schema: UserSchema },
      { name: admin.name, schema: AdminSchema },
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWTSECRET'),
        signOptions: {
          expiresIn: '1d',
        },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [AdminServiceService, LocalStrategy, JwtStrategy],
  controllers: [AdminController],
})
export class AdminModule {}
