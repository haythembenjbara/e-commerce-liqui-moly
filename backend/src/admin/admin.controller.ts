import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { CarParts } from '../models/car-parts.model';
import { CarPartsPipe } from './pipes/car_parts.pipe';
import { AdminServiceService } from './services/admin-service.service';
import { orderState, user } from 'src/models/user.model';
import { AuthGuard } from '@nestjs/passport';

@Controller({
  path: 'admin',
  version: '1',
})
export class AdminController {
  constructor(private adminService: AdminServiceService) {}

  // ********************* ADMIN AUTHENTIFICATION *********************
  @UseGuards(AuthGuard('local'))
  @Post('login')
  @HttpCode(HttpStatus.ACCEPTED)
  async login(
    @Body() body: { username: string; password: string },
  ): Promise<{ access_token: string }> {
    return this.adminService.login(body);
  }

  // ********************* CAR PARTS CRUD API *********************

  // ******** ADD API ********
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FilesInterceptor('image_urls', 5, {
      dest: './uploads',
    }),
  )
  @Post('addCarParts')
  @HttpCode(HttpStatus.CREATED)
  add_cars_parts(
    @Body(CarPartsPipe) cars_part: CarParts,
    @UploadedFiles() image_urls: Array<Express.Multer.File>,
  ): Promise<string> {
    return this.adminService.add_car_parts(cars_part, image_urls);
  }
  // ******** EDIT API ********
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FilesInterceptor('image_urls', 5, {
      dest: './uploads',
    }),
  )
  @Post('edit_car_parts/:product_id')
  @HttpCode(HttpStatus.OK)
  edit_car_parts(
    @Param('product_id') product_id: string,
    @Body(CarPartsPipe) cars_part: CarParts,
    @UploadedFiles() image_urls: Array<Express.Multer.File>,
  ): Promise<string> {
    return this.adminService.edit_car_parts(product_id, cars_part, image_urls);
  }
  // ******** DELETE API ********
  @UseGuards(AuthGuard('jwt'))
  @Delete('delete_car_parts/:product_id')
  @HttpCode(HttpStatus.OK)
  delete_car_parts(@Param('product_id') product_id: string): Promise<string> {
    return this.adminService.delete_car_parts(product_id);
  }

  // ********************* CAR CATEGORY CRUD API *********************

  // ******** ADD API ********
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FileInterceptor('image_url', {
      dest: './uploads',
    }),
  )
  @Post('addCarCategory')
  @HttpCode(HttpStatus.CREATED)
  add_cars_category(
    @Body() car_category,
    @UploadedFile() image: Express.Multer.File,
  ): Promise<string> {
    return this.adminService.add_car_category(car_category, image);
  }
  // ******** EDIT API ********
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FileInterceptor('image_url', {
      dest: './uploads',
    }),
  )
  @Post('edit_cars_category/:category_id')
  @HttpCode(HttpStatus.OK)
  edit_cars_category(
    @Param('category_id') category_id: string,
    @Body() car_category,
    @UploadedFile() image: Express.Multer.File,
  ): Promise<string> {
    return this.adminService.edit_car_category(
      category_id,
      car_category,
      image,
    );
  }
  // ******** DELETE API ********
  @UseGuards(AuthGuard('jwt'))
  @Delete('delete_car_category/:category_id')
  @HttpCode(HttpStatus.OK)
  delete_car_category(
    @Param('category_id') category_id: string,
  ): Promise<string> {
    return this.adminService.delete_car_category(category_id);
  }
  // ********************* CUSTOM CRUD OPERATION *********************
  @UseGuards(AuthGuard('jwt'))
  @Get('get_orders')
  @HttpCode(HttpStatus.OK)
  get_orders(): Promise<user[]> {
    return this.adminService.get_all_order();
  }
  @UseGuards(AuthGuard('jwt'))
  @Put('edit_order_state')
  @HttpCode(HttpStatus.OK)
  edit_order_state(
    @Body()
    body: {
      user_id: string;
      order_id: string;
      order_state: orderState;
    },
  ): Promise<string> {
    return this.adminService.edit_order_state(body);
  }
}
