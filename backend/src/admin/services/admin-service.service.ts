import {
  HttpException,
  HttpStatus,
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  CarCategory,
  CarCategoryDocument,
} from '../../models/car-category.model';
import { CarParts, CarPartsDocument } from '../../models/car-parts.model';
import { UserDocument, user } from 'src/models/user.model';
import { AdminDocument, admin } from 'src/models/admin.model';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AdminServiceService implements OnApplicationBootstrap {
  constructor(
    @InjectModel(CarParts.name) private carPartModel: Model<CarPartsDocument>,
    @InjectModel(CarCategory.name)
    private carCategoryModel: Model<CarCategoryDocument>,
    @InjectModel(user.name) private userModel: Model<UserDocument>,
    @InjectModel(admin.name) private adminModel: Model<AdminDocument>,
    private readonly configService: ConfigService,
    private jwtService: JwtService,
  ) {}

  async onApplicationBootstrap() {
    const countDocument = await this.adminModel.countDocuments();
    if (countDocument === 1) {
      return;
    }
    await this.adminModel.create({
      username: this.configService.get<string>('ADMIN_USERNAME'),
      password: await bcrypt.hash(
        this.configService.get<string>('ADMIN_PASSWORD'),
        10,
      ),
    });
  }
  // ADMIN AUTHENTIFICATION
  async validateAdmin(username: string, password: string): Promise<boolean> {
    const findAdmin = await this.adminModel.findOne({
      username: username,
    });
    if (!findAdmin) return false;
    return await bcrypt.compare(password, findAdmin.password);
  }
  async login({ username }): Promise<{ access_token: string }> {
    const findAdmin = await this.adminModel.findOne({
      username: username,
    });
    return {
      access_token: this.jwtService.sign({
        username: username,
        sub: findAdmin._id,
      }),
    };
  }
  // CAR PARTS CRUD
  public async add_car_parts(
    car_part: CarParts,
    images_url: Array<Express.Multer.File>,
  ): Promise<string> {
    const findCarPart = await this.carPartModel.findOne({
      name: car_part.name.toLowerCase(),
    });
    if (findCarPart) {
      throw new HttpException('CART PARTS EXIST', HttpStatus.CONFLICT);
    }
    car_part.image_urls = images_url.map((image) => image.filename);
    const added_car_parts = await this.carPartModel.create(car_part);
    await this.carCategoryModel.findOneAndUpdate(
      { name: car_part.category.name },
      { $push: { cars_part: added_car_parts } },
    );
    return 'ADDED';
  }
  public async edit_car_parts(
    product_id: string,
    car_part: CarParts,
    images_url: Array<Express.Multer.File>,
  ): Promise<string> {
    const old_products = await this.carPartModel.findById(product_id);
    const checkProduct = await this.carPartModel.findOne({
      _id: { $ne: product_id },
      name: car_part.name,
    });
    if (checkProduct) {
      throw new HttpException('CART PARTS EXIST', HttpStatus.CONFLICT);
    }
    if (images_url.length > 0) {
      car_part.image_urls = images_url.map((image) => image.filename);
    } else {
      car_part.image_urls = old_products.image_urls;
    }
    if (!car_part.sub_category) {
      car_part.sub_category = [];
    }
    const result = await this.carPartModel.updateOne(
      { _id: product_id },
      car_part,
    );
    if (result.modifiedCount === 0) {
      throw new HttpException('SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return 'EDIT SUCCESS';
  }
  public async delete_car_parts(product_id: string): Promise<string> {
    const result = await this.carPartModel.deleteOne({ _id: product_id });
    if (result.deletedCount === 0) {
      throw new HttpException('SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return 'SUCCESS';
  }
  // CAR CATEGORY CRUD
  public async add_car_category(
    car_category: CarCategory,
    image: Express.Multer.File,
  ): Promise<string> {
    const findCarCategory = await this.carCategoryModel.findOne({
      name: car_category.name.toLowerCase(),
    });
    if (findCarCategory) {
      throw new HttpException('CART CATEGORY EXIST', HttpStatus.CONFLICT);
    }
    car_category.image_url = image.filename;
    car_category.name = car_category.name.toLowerCase();
    await this.carCategoryModel.create(car_category);
    return 'ADDED';
  }
  public async edit_car_category(
    category_id: string,
    category: CarCategory,
    image: Express.Multer.File,
  ): Promise<string> {
    // CHECK IF NEW NAME ALEARDY EXIST
    const findByName = await this.carCategoryModel.findOne({
      _id: { $ne: category_id },
      name: category.name.toLowerCase(),
    });
    if (findByName) {
      throw new HttpException('CART CATEGORY EXIST', HttpStatus.CONFLICT);
    }
    if (image) {
      category.image_url = image.filename;
    } else {
      const oldCategory = await this.carCategoryModel.findById(category_id);
      category.image_url = oldCategory.image_url;
    }
    category.name = category.name.toLowerCase();
    const result = await this.carCategoryModel.updateOne(
      { _id: category_id },
      category,
    );
    if (result.modifiedCount === 0) {
      throw new HttpException('SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return 'EDIT SUCCESS';
  }
  public async delete_car_category(category_id: string): Promise<string> {
    const result = await this.carCategoryModel.deleteOne({ _id: category_id });
    if (result.deletedCount === 0) {
      throw new HttpException('SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return 'SUCCESS';
  }
  // RETRIEVE DATA :
  public async get_all_order(): Promise<user[]> {
    return this.userModel.find().populate({
      path: 'orders.order.car_part',
      model: CarParts.name,
    });
  }
  // CUSTOM CRUD OPERATION :
  public async edit_order_state({
    user_id,
    order_id,
    order_state,
  }): Promise<string> {
    const findUser = await this.userModel.findById(user_id);
    if (!findUser) {
      throw new HttpException('INVALID USER', HttpStatus.FORBIDDEN);
    }
    const result = await this.userModel.updateOne(
      {
        _id: user_id,
        'orders._id': order_id,
      },
      {
        $set: { 'orders.$.orderStatus': order_state },
      },
    );
    if (result.modifiedCount === 0) {
      throw new HttpException('SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return 'SUCCESS';
  }
}
