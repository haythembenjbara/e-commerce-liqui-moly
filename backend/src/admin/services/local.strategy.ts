import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AdminServiceService } from './admin-service.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private adminService: AdminServiceService) {
    super();
  }
  async validate(username: string, password: string): Promise<boolean> {
    const valid = await this.adminService.validateAdmin(username, password);
    if (!valid) {
      throw new UnauthorizedException();
    }
    return true;
  }
}
