import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { user, UserSchema } from '../models/user.model';
import { CarParts, CarPartsSchema } from '../models/car-parts.model';
import { CartController } from './cart.controller';
import { CartService } from './services/cart.service';
import { CheckoutController } from './checkout/checkout.controller';

@Module({
  controllers: [CartController, CheckoutController],
  imports: [
    MongooseModule.forFeature([
      { name: CarParts.name, schema: CarPartsSchema },
      { name: user.name, schema: UserSchema },
    ]),
  ],
  providers: [CartService],
})
export class CartModule {}
